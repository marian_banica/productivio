#include <QtCore/QDebug>
#include <QtCore/QThread>

#include "processmonitorworker.h"
#include "util.h"

#include <regex.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>

extern int errno;


char *ProcessMonitorWorker::browsers[]  = {"opera", "firefox", "chrome", "chromium", "rekonq", "safari"};
char *ProcessMonitorWorker::imClients[] = {"skype", "pidgin", "kopete", "empathy", "gajim"};


ProcessMonitorWorker::ProcessMonitorWorker(QHash<QString, long long> &browserUsageRef, QHash<QString, long long> &imClientsUsageRef, QHash<QString, ProcessInfo> &processesRef, QMutex &workerMutexRef)
    : browserUsage(browserUsageRef),
      imClientsUsage(imClientsUsageRef),
      processes(processesRef),
      workerMutex(workerMutexRef)
{
    processTypes.insert("browser", TypeInfo{browsers, sizeof(browsers) / sizeof(char *)});
    processTypes.insert("im-client", TypeInfo{imClients, sizeof(imClients) / sizeof(char *)});

    QObject::connect(&timer,    &QTimer::timeout,
                     this,      &ProcessMonitorWorker::scanProcesses);
    timer.setInterval(CHECK_INTERVAL * 1000);
}

ERR ProcessMonitorWorker::init()
{
    if(NULL == (display = XOpenDisplay(0)) )
    {
        qDebug() << "XOpenDisplay failed";
        return ERR_X_CONNECTION_FAILED;
    }

    utf8Atom            = XInternAtom(display, "UTF8_STRING", True);
    atomPID             = XInternAtom(display, "_NET_WM_PID", True);            // atom for property _NET_WM_PID (pid of the client owning the window)
    atomFocusID         = XInternAtom(display, "_NET_ACTIVE_WINDOW", True);     // atom for property _NET_ACTIVE_WINDOW (window id of the currently active window)
    atomName            = XInternAtom(display, "_NET_WM_NAME", True);           // atom for property _NET_WM_NAME (window name)
    atomActiveWindow    = XInternAtom(display, "_NET_ACTIVE_WINDOW", True);
    atomNetClientList   = XInternAtom(display, "_NET_CLIENT_LIST", True);

    return ERR_OK;
}

void ProcessMonitorWorker::run()
{
    ERR err;
    if( err = init() )
    {
        qDebug() << "init failed, err " << err;
        QThread::currentThread()->quit();
    }

    if(!timer.isActive())
        timer.start();
}

void ProcessMonitorWorker::stop()
{
    if(timer.isActive())
        timer.stop();

    if(NULL != display)
        XCloseDisplay(display);
    QThread::currentThread()->quit();
}

void ProcessMonitorWorker::scanProcesses()
{
    // Get the root window of the display
    Window baseWindow = XDefaultRootWindow(display);

    Window  *childrenWindows;
    Window  focusedWindow;

    Atom    retType;
    int     retFormat;
    ulong   retNumItems;
    ulong   retRemainingBytes;

    ERR     err;

    unsigned        pid;
    QString         processName;
    bool            isActive;

    unsigned char   *atomValue          = 0;
    unsigned char   *windowPid          = 0;
    unsigned char   *windowTitle        = 0;
    const char      *nameRegexString    = "Name:[^a-zA-Z]+([a-zA-Z]+)";

    unsigned char   *activeWindowValue;

    XGetWindowProperty(display,
                       baseWindow,
                       atomActiveWindow,
                       0, 1,
                       False,
                       XA_WINDOW,
                       &retType,
                       &retFormat,
                       &retNumItems,
                       &retRemainingBytes,
                       &activeWindowValue);

    if(activeWindowValue)
    {
        focusedWindow = *((Window*)activeWindowValue);
    }

    XGetWindowProperty(display,
                       baseWindow,
                       atomNetClientList,
                       0, 1024,
                       False,
                       XA_WINDOW,
                       &retType,
                       &retFormat,
                       &retNumItems,
                       &retRemainingBytes,
                       &atomValue);

    if(NULL == atomValue)
    {
        qDebug() << "no _NET_CLIENT_LIST property";
        QThread::currentThread()->quit();
    }

    ulong windowsCount = retNumItems;
    childrenWindows = (Window*)atomValue;

    QMutexLocker lock(&workerMutex);

    for(int i=0; i<windowsCount; i++)
    {

        XGetWindowProperty(display,
                           childrenWindows[i],
                           atomPID,
                           0, 1,
                           False,
                           XA_CARDINAL,
                           &retType,
                           &retFormat,
                           &retNumItems,
                           &retRemainingBytes,
                           &windowPid);

        if(NULL == windowPid)
            continue;

        isActive = (childrenWindows[i] == focusedWindow);

        pid = *((unsigned long *)windowPid);
        if( err = getProcessInfo(pid, nameRegexString, processName)){
            qDebug() << "getProcessInfo failed, err " << ERR_NO_MEMORY;
            if(ERR_NO_MEMORY == err){
                QThread::currentThread()->quit();
            }
        }

        if(!processes.contains(processName))
            processes.insert(processName, ProcessInfo{0, 0});
        else
        {
            processes[processName].duration += CHECK_INTERVAL;
            if(isActive)
                processes[processName].focusTime += CHECK_INTERVAL;
        }

        QHash<QString, long long> *container = NULL;

        if(processHasType(processName, "browser"))
        {
            container = &browserUsage;
        }
        else if(processHasType(processName, "im-client"))
        {
            container = &imClientsUsage;
        }

        if(NULL != container)
        {
            XGetWindowProperty(display,
                               childrenWindows[i],
                               atomName,
                               0, 65536,
                               False,
                               utf8Atom,
                               &retType,
                               &retFormat,
                               &retNumItems,
                               &retRemainingBytes,
                               &windowTitle);

            if(NULL != windowTitle)
            {
                QString key((char*)windowTitle);

                if(!container->contains(key))
                {
                    container->insert(key, isActive ? CHECK_INTERVAL : 0);
                }
                else if(isActive)
                {
                    (*container)[key] += CHECK_INTERVAL;
                }
                XFree(windowTitle);
            }
        }

        XFree(windowPid);
    }

    XFree(atomValue);
}


ERR ProcessMonitorWorker::getProcessInfo(uint pid, const char *propRegex, QString &value)
{
    const uint  maxProcPath = 20;
    const uint  bufferSize  = 500;
    char        *procPath;
    char        *propValue;
    char        *buffer;
    int         fd;
    size_t      size;
    regex_t     regex;
    regmatch_t  regexMatch[2];
    ERR         err;

    procPath = (char*)calloc(maxProcPath, sizeof(char));
    if(NULL == procPath)
    {
        qDebug() << "No memory";
        return ERR_NO_MEMORY;
    }

    sprintf(procPath, "/proc/%d/status", pid);
    fd = open(procPath, O_RDONLY);
    free(procPath);

    if(fd < 0)
    {
        qDebug() << "open failed, errno " << errno;
        return ERR_SYS_FUN;
    }

    buffer = (char*)calloc(bufferSize, 1);
    if(NULL == buffer)
    {
        qDebug() << "No memory";
        return ERR_NO_MEMORY;
    }

    size = read(fd, buffer, bufferSize);
    close(fd);

    if( err = regcomp(&regex, propRegex, REG_EXTENDED) )
    {
        qDebug() << "regcomp() failed, err " << err;
        return ERR_SYS_FUN;
    }

    if( err = regexec(&regex, buffer, 2, regexMatch, 0) )
    {
        free(buffer);
        regfree(&regex);
        qDebug() << "regexec() failed, err " << err;
        return ERR_SYS_FUN;
    }

    size = regexMatch[1].rm_eo - regexMatch[1].rm_so;
    propValue = (char *)calloc(size, 1);
    if(NULL == propValue)
    {
        free(buffer);
        regfree(&regex);
        qDebug() << "No memory";
        return ERR_NO_MEMORY;
    }

    memcpy(propValue, buffer + regexMatch[1].rm_so, size);

    value = propValue;

    free(buffer);
    free(propValue);
    regfree(&regex);

    return ERR_OK;
}

bool ProcessMonitorWorker::processHasType(QString &processName, const char* type)
{
    if(!processTypes.contains(type))
    {
        qDebug() << "ProcessMonitorWorker::processHasType, invalid type";
        return false;
    }

    TypeInfo    &typeInfo= processTypes[type];
    return itemIsOfType(processName, typeInfo.data, typeInfo.len);
}





