include(../common.pri);

TEMPLATE    = lib

TARGET      = systemmonitor
DESTDIR     = $$LIB_PATH

LIBS        += -L/usr/lib64 -lX11

QT          = core dbus

SOURCES     += \
    systemmonitor.cpp \
    processmonitor.cpp \
    processmonitorworker.cpp \
    documentmonitor.cpp \
    documentmonitorworker.cpp \
    util.cpp \
    idlemonitor.cpp

HEADERS += \
    systemmonitor.h \
    BaseMonitor.h \
    processmonitor.h \
    processmonitorworker.h \
    documentmonitor.h \
    documentmonitorworker.h \
    util.h \
    idlemonitor.h
