#ifndef SYSTEMMONITOR_H
#define SYSTEMMONITOR_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QList>
#include <QtCore/QTimer>

#include "BaseMonitor.h"


class SystemMonitor: public QObject{

    Q_OBJECT

private:
    SystemMonitor();
    static SystemMonitor    *_instance;
    QList<BaseMonitor*>     monitors;
    QTimer      timer;
    QDateTime   from;

public:
    static SystemMonitor *instance();

    void    start();
    void    stop();

    QList<BaseMonitor*>     *getMonitors();

public slots:
    void    report();

signals:
    void    settingsUpdated();
};


#endif // SYSTEMMONITOR_H
