#include "documentmonitorworker.h"
#include "util.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QThread>

#include <sys/inotify.h>
#include <stdio.h>
#include <errno.h>
#include <sys/select.h>
#include <memory.h>
#include <unistd.h>

#define MAX_PATH_LEN    500
#define SELECT_TIMEOUT  3

extern int errno;


char *DocumentMonitorWorker::watchedDocuments[]  = {"\.pdf$", "\.nfo$", "\.txt$", "\.doc$", "\.docx$", "\.chm$", "\.log$"};


DocumentMonitorWorker::DocumentMonitorWorker(QList<DocumentUsageInfo> &documentsUsageRef, QMutex &workerMutexRef)
    :running(false),
      documentsUsage(documentsUsageRef),
      workerMutex(workerMutexRef)
{
}

void DocumentMonitorWorker::run()
{
    ERR err;

    if(err = init())
    {
        qDebug() << "init failed, err " << err;
        QThread::currentThread()->quit();
    }

    fd_set descriptors;
    FD_ZERO(&descriptors);
    FD_SET(notify_fd, &descriptors);

    struct  inotify_event notify_event;
    int     ready = 0;
    ssize_t data_read;

    struct timeval timeout;
    timeout.tv_sec = SELECT_TIMEOUT;

    while(running)
    {
        ready = select(notify_fd + 1,
                       &descriptors,
                       NULL,
                       NULL,
                       &timeout);

        FD_ZERO(&descriptors);
        FD_SET(notify_fd, &descriptors);

        timeout.tv_sec = SELECT_TIMEOUT;
        timeout.tv_usec = 0;

        if(0 == ready)
            continue;

        if(-1 == ready)
        {
            qDebug() << "select failed, err " << errno;
            break;
        }

        if(ready > 0)
        {
            memset(&notify_event, 0, sizeof(notify_event));
            data_read = read(notify_fd, (void*)(&notify_event), 200);
            if(data_read < 0){
                qDebug() << "read failed, err " << errno;
                continue;
            }

            QMutexLocker locker(&workerMutex);
            QString item(notify_event.name);
            if(notify_event.mask == IN_ACCESS && itemIsOfType(item, watchedDocuments, sizeof(watchedDocuments) / sizeof(char *)))
            {
                int documentsUsageCount = documentsUsage.count();
                if (documentsUsageCount > 0 && documentsUsage.at(documentsUsageCount - 1).documentName == item && documentsUsage.at(documentsUsageCount - 1).accessTime.secsTo(QDateTime::currentDateTime()) < 2)
                    continue;

                documentsUsage.append(DocumentUsageInfo{item, QDateTime::currentDateTime()});
            }
        }
    }

    inotify_rm_watch(notify_fd, wd);
    close(notify_fd);

    QThread::currentThread()->quit();
}

ERR DocumentMonitorWorker::init()
{
    notify_fd = inotify_init();
    if(-1 == notify_fd){
        qDebug() << "inotify_init failed, err " << errno;
        return ERR_SYS_FUN;
    }

    installDocumentMonitors(QDir::homePath().toLocal8Bit().data());

    return ERR_OK;
}

void DocumentMonitorWorker::installDocumentMonitors(char *path)
{
    // add watch on dir
    int wd = inotify_add_watch(notify_fd, path, IN_ACCESS);
    if(-1 == wd)
    {
        qDebug() << "inotify_add_watch failed, path " << path << ", errno " << errno;
        return;
    }

    DIR *dir;
    if( NULL == (dir = opendir(path) ) )
    {
        qDebug() << "opendir failed, path " << path << ", errno " << errno;
        return;
    }

    struct dirent *dirEntry = readdir(dir);

    while(NULL != dirEntry)
    {
        if(dirEntry->d_type != DT_DIR || 0 == strcmp(dirEntry->d_name, ".") || 0 == strcmp(dirEntry->d_name, "..") )
        {
            dirEntry = readdir(dir);
            continue;
        }

        char    *dirPath = (char*)calloc(MAX_PATH_LEN, 1);
        int     len;

        len = sprintf(dirPath, "%s/%s", path, dirEntry->d_name );
        dirPath[len] = '\0';

        installDocumentMonitors(dirPath);

        free(dirPath);
        dirEntry = readdir(dir);
    }

    closedir(dir);
}
