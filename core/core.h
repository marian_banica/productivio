
typedef int ERR;

#define     ERR_OK                      0
#define     ERR_SYS_FUN                 1
#define     ERR_INVALID_PARAM           2
#define     ERR_NO_MEMORY               3
#define     ERR_PROC_FAILED             4
#define     ERR_X_CONNECTION_FAILED     5
#define     ERR_X_FUNCTION_FAILED       6

