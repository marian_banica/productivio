#include "idlemonitor.h"
#include <QtDBus>

IdleMonitor *IdleMonitor::_instance = NULL;

IdleMonitor::IdleMonitor():
    idleDuration(0),
    isIdle(false)
{    
    QObject::connect(&checkTimer,   &QTimer::timeout,
                     this,          &IdleMonitor::checkIdleTime);
}

IdleMonitor *IdleMonitor::instance()
{
    if(NULL == _instance)
        _instance = new IdleMonitor();
    return _instance;
}

ERR IdleMonitor::start()
{
    if(STATE_RUNNING == state)
        return ERR_OK;

    checkTimer.setInterval(CHECK_INTERVAL * 1000);
    checkTimer.start();

    setState(STATE_RUNNING);
    return ERR_OK;
}

ERR IdleMonitor::stop()
{
    if(STATE_RUNNING != state)
        return ERR_OK;

    checkTimer.stop();
    setState(STATE_STOPPED);

    return ERR_OK;
}

void IdleMonitor::checkIdleTime()
{
    QDBusMessage message = QDBusMessage::createMethodCall("org.freedesktop.ScreenSaver", "/ScreenSaver", "org.freedesktop.ScreenSaver", "GetSessionIdleTime");
    QDBusMessage response = QDBusConnection::sessionBus().call(message);

    if(response.type() != QDBusMessage::ReplyMessage)
    {
        qDebug() << "bad reply type, " << response.type();
        return;
    }

    QList<QVariant> responseArgs = response.arguments();
    if(responseArgs.count() != 1)
    {
        qDebug() << "bad reply args count";
        return;
    }

    idleDuration = responseArgs.at(0).toInt() / 1000;
    if(idleDuration >= IDLE_TIME)
    {
        if(!isIdle)
        {
            isIdle = true;
            idleStart = QDateTime::currentDateTime().addSecs(-idleDuration);
        }
    }
    else if(isIdle)
    {
        isIdle = false;
        idleTimes.append(IdleInfo{ idleStart, prevIdleDuration });
    }

    prevIdleDuration = idleDuration;
}

void IdleMonitor::report(QJsonObject &parent)
{
    QJsonArray jsonIdleTimes;

    for(QList<IdleInfo>::iterator it = idleTimes.begin(); it != idleTimes.end(); it ++)
    {
        QJsonObject obj;
        obj.insert("duration", QJsonValue::fromVariant( (*it).duration ));
        obj.insert("start_time", QJsonValue::fromVariant( (*it).startTime ));
        jsonIdleTimes.append(obj);
    }

    if(isIdle)
    {
        QJsonObject obj;
        obj.insert("duration", QJsonValue::fromVariant( idleDuration ));
        obj.insert("start_time", QJsonValue::fromVariant( QDateTime::currentDateTime().addSecs(-idleDuration) ));
        jsonIdleTimes.append(obj);
        idleDuration = 0;
    }

    parent.insert("idle_monitor", jsonIdleTimes);
    idleTimes.clear();
}
