#include "reportingsystem.h"

#include <QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QUrl>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include "../app/settings.h"


#define SERVER_ADDRESS          "http://localhost:8080"
#define METHOD_POST_ACTIVITIES  "activities"
#define PENDING_FILE_NAME       "productivio.dat"


ReportingSystem *ReportingSystem::_instance = NULL;


ReportingSystem::ReportingSystem():
    waitingForReply(false),
    lastRequestData(NULL)
{
    QObject::connect(&netManager,   &QNetworkAccessManager::finished,
                     this,          &ReportingSystem::receivedReply);
    pendingRequestsFile.setFileName(QString("%1/%2").arg(QCoreApplication::applicationDirPath()).arg(PENDING_FILE_NAME));
    pendingRequestsFile.open(QIODevice::Append | QIODevice::ReadWrite);
}

ReportingSystem *ReportingSystem::instance()
{
    if(NULL == _instance)
        _instance = new ReportingSystem();
    return _instance;
}

void ReportingSystem::report(QJsonObject activity)
{
    // TODO: add signature to activity
    QJsonDocument   document;
    QByteArray      *postData;

    document.setObject(activity);
    postData = new QByteArray(document.toJson(QJsonDocument::Compact));

    if(waitingForReply){
        sendingQueue.enqueue(postData);
    }else{
        waitingForReply = true;
        sendRequest(postData);
    }
}

void ReportingSystem::receivedReply(QNetworkReply *reply)
{
    if(!reply->isFinished())
        return;

    waitingForReply = false;

    if(reply->error() != QNetworkReply::NoError)
    {
        qDebug() << "post failed, reply " <<  reply->error();
        pendingRequestsFile.write(*lastRequestData);
        pendingRequestsFile.flush();

        qlonglong pendingRequests = settings->value("pending_reports", 0).toLongLong();
        settings->setValue("pending_reports", ++ pendingRequests);

        delete lastRequestData;
    }

    emit reported();

    if(!sendingQueue.isEmpty())
        sendRequest(sendingQueue.dequeue());
}

void ReportingSystem::sendRequest(QByteArray *data)
{
    lastRequestData = data;
    QNetworkRequest netRequest(QUrl(QString("%1?%2").arg(SERVER_ADDRESS).arg(METHOD_POST_ACTIVITIES)));
    netManager.post(netRequest, *lastRequestData);
}


ReportingSystem::~ReportingSystem()
{
    pendingRequestsFile.close();
}
