#ifndef DOCUMENTMONITOR_H
#define DOCUMENTMONITOR_H

#include "BaseMonitor.h"
#include "documentmonitorworker.h"

class DocumentMonitor: public BaseMonitor
{
    Q_OBJECT

public:
    static DocumentMonitor *instance();

    void    report(QJsonObject &);
    ERR     start();
    ERR     stop();

private:
    DocumentMonitor();
    static DocumentMonitor      *_instance;
    DocumentMonitorWorker       worker;
    QMutex                      workerMutex;
    QList<DocumentUsageInfo>    documentsUsage;
};

#endif // DOCUMENTMONITOR_H





