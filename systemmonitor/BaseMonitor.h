#ifndef BASEMONITOR_H
#define BASEMONITOR_H

#include <QtCore/QObject>
#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>
#include <QtCore/QDateTime>
#include "core.h"


typedef enum{
    STATE_RUNNING,
    STATE_STOPPING,
    STATE_STOPPED,
    STATE_ERROR
}STATE;


class BaseMonitor: public QObject{
    Q_OBJECT

public:
    BaseMonitor():
        state(STATE_STOPPED) {

        QObject::connect(&workerThread,     &QThread::finished,
                         this,              &BaseMonitor::workerThreadFinished);

    }

    void init(QString name, QString icon)
    {
        this->name = name;
        this->icon = icon;
    }

    QString getName()
    {
        return name;
    }

    QString getIcon()
    {
        return icon;
    }

    STATE getState()
    {
        return state;
    }

    virtual ERR     start() = 0;
    virtual ERR     stop()  = 0;
    virtual void    report(QJsonObject &) = 0;

protected:

    void setState(STATE newState)
    {
        state = newState;
        emit stateChanged(name, state);
    }

    STATE       state;
    QString     name;
    QString     icon;
    QDateTime   startTime;
    QThread     workerThread;
    QMutex      workerMutex;

public slots:
    void toggleState(){
        if(state == STATE_STOPPING)
            return;

        if(state == STATE_RUNNING)
            stop();
        else
            start();
    }

private slots:
    void workerThreadFinished()
    {
        if(state == STATE_STOPPING)
        {
            setState(STATE_STOPPED);
        }
        else
        {
            qDebug() << name << " worker crashed";
            setState(STATE_ERROR);
        }
    }

signals:
    void stateChanged(QString name, STATE state);
    void stopWorker();
};


#endif // BASEMONITOR_H
