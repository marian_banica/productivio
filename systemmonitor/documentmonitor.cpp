#include "documentmonitor.h"
#include <QtCore/QDebug>

DocumentMonitor *DocumentMonitor::_instance = NULL;

DocumentMonitor::DocumentMonitor()
    :worker(documentsUsage, workerMutex)
{
    QObject::connect(&workerThread, &QThread::started,
                     &worker,       &DocumentMonitorWorker::run);
    worker.moveToThread(&workerThread);
}

DocumentMonitor *DocumentMonitor::instance()
{
    if(NULL == _instance)
        _instance = new DocumentMonitor();
    return _instance;
}

ERR DocumentMonitor::start()
{
    setState(STATE_RUNNING);

    worker.running = true;
    workerThread.start();

    return ERR_OK;
}

ERR DocumentMonitor::stop()
{
    if(STATE_RUNNING != state)
        return ERR_OK;

    setState(STATE_STOPPING);
    worker.running = false;

    return ERR_OK;
}

void DocumentMonitor::report(QJsonObject &parent)
{
    QMutexLocker locker(&workerMutex);
    QJsonArray jsonDocumentsUsage;

    for(QList<DocumentUsageInfo>::iterator it = documentsUsage.begin(); it != documentsUsage.end(); it ++)
    {
        QJsonObject obj;
        obj.insert("document", (*it).documentName );
        obj.insert("access_time", QJsonValue::fromVariant((*it).accessTime) );
        jsonDocumentsUsage.append(obj);
    }

    parent.insert("document_monitor", jsonDocumentsUsage);
    documentsUsage.clear();
}
