#ifndef DOCUMENTMONITORWORKER_H
#define DOCUMENTMONITORWORKER_H

#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include "core.h"

#include <dirent.h>

typedef struct
{
    QString     documentName;
    QDateTime   accessTime;
}DocumentUsageInfo;

class DocumentMonitorWorker: public QObject
{

    Q_OBJECT

public:
    DocumentMonitorWorker(QList<DocumentUsageInfo> &, QMutex &);
    bool running;

public slots:
    void run();

private:
    void    installDocumentMonitors(char *path);
    ERR     init();

    int     wd;         // inotify watch descriptor
    int     notify_fd;  // inotify instance descriptor

    static char *watchedDocuments[];

    QList<DocumentUsageInfo>    &documentsUsage;
    QMutex                      &workerMutex;

};

#endif // DOCUMENTMONITORWORKER_H
