#include "loghandler.h"

#include <QCoreApplication>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>

#define LOG_FILE        "debug.log"
#define LOG_PATH_LEN    200
#define DBG_MSG_LEN     1000

void globalMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
   LogHandler::instance()->debugHandler(type, context, msg);
}


LogHandler *LogHandler::_instance = NULL;


LogHandler::LogHandler()
{
    char *location = (char*)malloc(LOG_PATH_LEN);
    int size = sprintf(location, "%s/%s", QCoreApplication::applicationDirPath().toStdString().c_str(), LOG_FILE);
    location[size] = '\0';

    fd = open(location, O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    free(location);

    qInstallMessageHandler(globalMessageHandler);
}

LogHandler *LogHandler::instance()
{
    if(NULL == _instance)
        _instance = new LogHandler();
    return _instance;
}

LogHandler::~LogHandler()
{
    if(fd > 0)
        close(fd);
}

void LogHandler::debugHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    switch (type) {
    case QtDebugMsg:
        if(fd > 0)
        {
            QString output = QString("[%1] %2:L%3 %4 \n").arg(QDateTime::currentDateTime().toString(Qt::ISODate), context.function, QString::number(context.line), msg);
            write(fd, output.toLocal8Bit().data(), output.size());
        }
        break;
    }
}
