#include <QtCore/QDebug>
#include <QtCore/QProcess>

#include "processmonitor.h"


ProcessMonitor *ProcessMonitor::_instance = NULL;


ProcessMonitor::ProcessMonitor()
    : worker(browserUsage, imClientsUsage, processes, workerMutex)
{
    worker.moveToThread(&workerThread);
    QObject::connect(&workerThread, &QThread::started,
                     &worker,       &ProcessMonitorWorker::run);
    QObject::connect(this, &BaseMonitor::stopWorker,
                     &worker, &ProcessMonitorWorker::stop);
}

ProcessMonitor *ProcessMonitor::instance()
{
    if(NULL == _instance)
        _instance = new ProcessMonitor();
    return _instance;
}

ERR ProcessMonitor::start()
{
    QString username;
    QString uid;
    ERR     err;

    if(STATE_RUNNING == state)
        return ERR_OK;

    if(err = getUserName(username))
    {
        qDebug() << "getUserName failed, err " << err;
        return err;
    }

    if(err = getUserId(username, uid))
    {
        qDebug() << "getUserId failed, err " << err;
        return err;
    }

    setState(STATE_RUNNING);
    workerThread.start();

    return ERR_OK;
}

ERR ProcessMonitor::stop()
{
    if(STATE_RUNNING == state)
    {
        setState(STATE_STOPPING);
        stopWorker();
    }
    else
    {
        return ERR_OK;
    }

    return ERR_OK;
}

/*
 *  ! Get user name
 *  The user's name is used for getting the user's ID
 */

ERR ProcessMonitor::getUserName(QString &username)
{
    QProcess process;
    process.start("whoami");
    if(!process.waitForReadyRead())
    {
        return ERR_PROC_FAILED;
    }
    username = process.readLine().trimmed();
    process.waitForFinished();

    return ERR_OK;
}

/*
 *  ! Get user ID
 *  The user's ID is used when filtering running processes
 */

ERR ProcessMonitor::getUserId(QString &username, QString &uid)
{
    QProcess    process;
    QStringList args;

    args << "-u" << username;

    process.start("id", args);
    if(!process.waitForReadyRead())
    {
        return ERR_PROC_FAILED;
    }
    uid = process.readLine().trimmed();
    process.waitForFinished();

    return ERR_OK;
}

void ProcessMonitor::report(QJsonObject &parent)
{
    QMutexLocker locker(&workerMutex);

    QJsonArray jsonBrowserUsage;
    QJsonArray jsonImClientsUsage;
    QJsonArray jsonProcesses;

    // browsers

    for(QHash<QString, long long>::Iterator it = browserUsage.begin(); it != browserUsage.end(); it++)
    {
        QJsonObject obj;
        obj.insert("focus_time", QJsonValue::fromVariant(QVariant::fromValue(it.value())));
        obj.insert("name", it.key());
        jsonBrowserUsage.append(obj);
    }
    parent.insert("browsers", jsonBrowserUsage);

    // im-clients

    for(QHash<QString, long long>::Iterator it = imClientsUsage.begin(); it != imClientsUsage.end(); it++)
    {
        QJsonObject obj;
        obj.insert("focus_time", QJsonValue::fromVariant(QVariant::fromValue(it.value())));
        obj.insert("name", it.key());
        jsonImClientsUsage.append(obj);
    }
    parent.insert("im-clients", jsonImClientsUsage);

    // processes

    for(QHash<QString, ProcessInfo>::Iterator it = processes.begin(); it != processes.end(); it++)
    {
        QJsonObject obj;
        obj.insert("duration", QJsonValue::fromVariant(QVariant::fromValue(it.value().duration)));
        obj.insert("focus_time", QJsonValue::fromVariant(QVariant::fromValue(it.value().focusTime)));
        obj.insert("name", it.key());
        jsonProcesses.append(obj);
    }

    parent.insert("process_monitor", jsonProcesses);

    browserUsage.clear();
    imClientsUsage.clear();
    processes.clear();
}
