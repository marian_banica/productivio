#include <QtCore/QDebug>

#include "systemmonitor.h"
#include "processmonitor.h"
#include "documentmonitor.h"
#include "idlemonitor.h"

#include "../reportingsystem/reportingsystem.h"
#include "../app/settings.h"


#define REPORT_INTERVAL 600000      // report every 10 minutes


SystemMonitor *SystemMonitor::_instance = NULL;


SystemMonitor::SystemMonitor()
{
    ProcessMonitor::instance()->init("Process Monitor", ":/data/img/process_monitor.png");
    DocumentMonitor::instance()->init("Document Monitor", ":/data/img/filesystem_monitor.png");
    IdleMonitor::instance()->init("Idle Monitor", ":/data/img/idle_monitor.png");
    monitors.append( ProcessMonitor::instance() );
    monitors.append( DocumentMonitor::instance() );
    monitors.append( IdleMonitor::instance() );

    QObject::connect(&timer,    &QTimer::timeout,
                     this,      &SystemMonitor::report);
    timer.setInterval(REPORT_INTERVAL);
}

SystemMonitor *SystemMonitor::instance()
{
    if(NULL == _instance)
        _instance = new SystemMonitor();
    return _instance;
}

void SystemMonitor::start()
{
    ERR err;

    for(int i=0; i<monitors.length(); i++)
    {
        if( err = monitors[i]->start())
            qDebug() << monitors[i]->getName() << " start failed, err " << err;
    }

    settings->setValue("next_report", QDateTime::currentDateTime().addMSecs(REPORT_INTERVAL));
    emit settingsUpdated();

    from = QDateTime::currentDateTime();
    timer.start();
}

void SystemMonitor::stop()
{
    foreach(BaseMonitor *monitor, monitors)
        monitor->stop();

    timer.stop();
    // TODO report
}

QList<BaseMonitor *> *SystemMonitor::getMonitors()
{
    return &monitors;
}

void SystemMonitor::report()
{
    QJsonObject     activity;
    QJsonObject     data;

    foreach(BaseMonitor *monitor, monitors)
    {
        monitor->report(data);
    }

    activity.insert("data", data);
    activity.insert("from", QJsonValue::fromVariant(from));
    activity.insert("to", QJsonValue::fromVariant(QDateTime::currentDateTime()));

    settings->setValue("next_report", QDateTime::currentDateTime().addMSecs(REPORT_INTERVAL));
    settings->setValue("last_report", QDateTime::currentDateTime());

    ReportingSystem::instance()->report(activity);
}
