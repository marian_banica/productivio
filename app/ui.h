#ifndef UI_H
#define UI_H

#include <QtCore/QHash>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QAbstractButton>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QIcon>

#include "../systemmonitor/systemmonitor.h"
#include "../reportingsystem/reportingsystem.h"


struct MonitorStatus{
    bool        enabled;
    QLabel      *icon;
    QPushButton *container;
};


class Ui: public QMainWindow
{
    Q_OBJECT

public:
    Ui(QWidget *parent = NULL);
    void bind(SystemMonitor *systemMonitor, ReportingSystem *reportingSystem);

private:
    void buildUi();

    QHBoxLayout *monitorsLayout;
    QLabel *lastReportLabel;
    QLabel *nextReportLabel;
    QLabel *pendingReportsLabel;
    QLabel *versionLabel;
    QLabel *updateButton;
    QHash<QString, MonitorStatus> monitorStatuses;

private slots:
    void monitorStateChanged(QString monitorName, STATE state);
    void updateStatus();

};

#endif // UI_H
