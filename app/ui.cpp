#include "ui.h"
#include "../systemmonitor/systemmonitor.h"
#include <QtCore/QSettings>

extern QSettings *settings;

Ui::Ui(QWidget *parent)
    :QMainWindow(parent)
{

    setWindowTitle("Productivio");
    setMinimumHeight(400);
    setMinimumWidth(620);

    setWindowIcon(QIcon(":/data/img/app-icon.png"));

    buildUi();
    this->show();
}

void Ui::buildUi()
{
    QVBoxLayout *containerLayout    = new QVBoxLayout(this);
    QHBoxLayout *wrapper            = new QHBoxLayout(this);
    QWidget     *statusContainer    = new QFrame(this);
    QVBoxLayout *statusLayout       = new QVBoxLayout(this);
    QStatusBar  *statusBar          = new QStatusBar(this);

    monitorsLayout    = new QHBoxLayout();

    wrapper->addStretch();
    wrapper->addLayout(monitorsLayout);
    wrapper->addStretch();

    containerLayout->setSpacing(15);
    containerLayout->addSpacing(55);
    containerLayout->addLayout(wrapper);
    containerLayout->addStretch();
    containerLayout->addWidget(statusContainer);
    containerLayout->addStretch();

    lastReportLabel = new QLabel();
    nextReportLabel = new QLabel();
    pendingReportsLabel = new QLabel();

    updateStatus();

    statusContainer->setStyleSheet(".QLabel{color: #555; margin-left: 20px; margin-bottom: 10px}");
    statusLayout->addWidget(lastReportLabel);
    statusLayout->addWidget(nextReportLabel);
    statusLayout->addWidget(pendingReportsLabel);
    statusContainer->setLayout(statusLayout);

    versionLabel = new QLabel();
    updateButton = new QLabel();
    versionLabel->setStyleSheet(".QLabel{margin-left:10px}");
    updateButton->setStyleSheet(".QLabel{color:#25f}");
    versionLabel->setText("Version: 1.0");

    statusBar->setStyleSheet(".QStatusBar{background: #fff; border-top: 1px solid #ddd}");
    statusBar->setMinimumHeight(30);
    statusBar->addWidget(versionLabel);
    statusBar->addWidget(updateButton);

    setStatusBar(statusBar);

    setCentralWidget(new QWidget(this));
    centralWidget()->setLayout(containerLayout);
}

void Ui::monitorStateChanged(QString monitorName, STATE state)
{
    if(!monitorStatuses.contains(monitorName))
        return;

    MonitorStatus &monitorStatus = monitorStatuses[monitorName];

    switch(state)
    {
        case STATE_RUNNING:
            monitorStatus.icon->setPixmap(QPixmap(":/data/img/monitor_on.png"));
            monitorStatus.container->setStyleSheet(".QPushButton {background: #fdfdfd; border:1px solid #ddd; border-radius: 3px}");
            monitorStatus.enabled = true;
            break;
        case STATE_STOPPED:
            monitorStatus.icon->setPixmap(QPixmap(":/data/img/monitor_off.png"));
            monitorStatus.container->setStyleSheet(".QPushButton {background: #fdfdfd; border:1px solid #ddd; border-radius: 3px}");
            monitorStatus.enabled = true;
            break;
        case STATE_ERROR:
            monitorStatus.icon->setPixmap(QPixmap(":/data/img/monitor_error.png"));
            monitorStatus.container->setStyleSheet(".QPushButton {background: #fdfdfd; border:1px solid #ddd; border-radius: 3px}");
            monitorStatus.enabled = true;
            break;
        case STATE_STOPPING:
            monitorStatus.enabled = false;
            monitorStatus.container->setStyleSheet(".QPushButton {background: #f1f1f1; border:1px solid #ddd; border-radius: 3px}");
            break;
    }
}

void Ui::updateStatus()
{
    qlonglong pendingReports = settings->value("pending_reports", 0).toLongLong();
    QVariant lastReportVariant = settings->value("last_report");
    QVariant nextReportVariant = settings->value("next_report");

    if(0 == pendingReports)
    {
        pendingReportsLabel->setText("No pending reports");
        pendingReportsLabel->setStyleSheet("*{color:green}");
    }
    else
    {
        pendingReportsLabel->setText(QString("%1 pending reports").arg(pendingReports));
        pendingReportsLabel->setStyleSheet("*{color:red}");
    }

    if( lastReportVariant.isNull() )
    {
         lastReportLabel->setText("Last Report: Never");
    }
    else
    {
        lastReportLabel->setText(QString("Last Report: %1").arg(lastReportVariant.toDateTime().toString(Qt::SystemLocaleShortDate)));
    }

    if( nextReportVariant.isNull() )
    {
         nextReportLabel->setText("Next Report: -");
    }
    else
    {
        nextReportLabel->setText(QString("Next Report: %1").arg(nextReportVariant.toDateTime().toString(Qt::SystemLocaleShortDate)));
    }
}

void Ui::bind(SystemMonitor *systemMonitor, ReportingSystem *reportingSystem)
{

    QList<BaseMonitor*> *monitors = systemMonitor->getMonitors();

    for(int i=0; i<monitors->count(); i++)
    {
        QPushButton *monitorContainer   = new QPushButton();
        QVBoxLayout *monitorLayout      = new QVBoxLayout();
        QLabel      *monitorName        = new QLabel();
        QLabel      *monitorIcon        = new QLabel();
        QLabel      *monitorStatusIcon  = new QLabel(monitorContainer);
        BaseMonitor *monitor = monitors->at(i);

        monitorStatuses.insert(monitor->getName(), MonitorStatus{false, monitorStatusIcon, monitorContainer});

        monitorContainer->setLayout(monitorLayout);
        monitorContainer->setFixedSize(125,125);
        monitorName->setText(monitor->getName());
        monitorName->setFont(QFont("Helvetica", 9 ));
        monitorIcon->setPixmap(QPixmap(monitor->getIcon()));
        monitorStatusIcon->setPixmap(QPixmap(":/data/img/monitor_off.png"));
        monitorStatusIcon->move(82,5);
        monitorLayout->addWidget(monitorIcon);
        monitorLayout->addWidget(monitorName);
        monitorsLayout->addWidget(monitorContainer);

        QObject::connect(monitor,   &BaseMonitor::stateChanged,
                         this,      &Ui::monitorStateChanged);
        QObject::connect(monitorContainer,  &QPushButton::clicked,
                         monitor,           &BaseMonitor::toggleState);
        monitorStateChanged(monitor->getName(), monitor->getState());

        monitorName->setAlignment(Qt::AlignCenter);
        monitorName->setStyleSheet(".QLabel {color: #555; }");
        monitorIcon->setStyleSheet(".QLabel {margin-left: 30px; margin-top:35px}");
        monitorContainer->setStyleSheet(".QPushButton {background: #fdfdfd; border:1px solid #ddd; border-radius: 3px}");
    }

    QObject::connect(reportingSystem, &ReportingSystem::reported,
                     this, &Ui::updateStatus);

    QObject::connect(systemMonitor, &SystemMonitor::settingsUpdated,
                     this, &Ui::updateStatus);

}
