#ifndef LOGHANDLER_H
#define LOGHANDLER_H

#include <QtCore/QDebug>
#include <QtCore/QDateTime>


class LogHandler
{
public:
    static LogHandler *instance();
    ~LogHandler();
    void debugHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

private:
    LogHandler();
    static LogHandler *_instance;

    int fd;
};

#endif // LOGHANDLER_H
