#ifndef IDLEMONITOR_H
#define IDLEMONITOR_H

#include "BaseMonitor.h"

#include <QtCore/QTimer>
#include <QtCore/QList>
#include <QtCore/QDateTime>


#define IDLE_TIME       5   // if inactive for IDLE_TIME seconds, consider idle
#define CHECK_INTERVAL  3   // check idle time every CHECK_INTERVAL seconds


typedef struct
{
    QDateTime   startTime;
    long long   duration;
}IdleInfo;


class IdleMonitor: public BaseMonitor
{
    Q_OBJECT


public:
    static IdleMonitor *instance();

    void            report(QJsonObject &);
    ERR             start();
    ERR             stop();

private:
    IdleMonitor();
    static IdleMonitor *_instance;

    QList<IdleInfo> idleTimes;
    QTimer          checkTimer;
    QDateTime       idleStart;
    long long       idleDuration;
    long long       prevIdleDuration;
    bool            isIdle;

private slots:
    void checkIdleTime();

};

#endif // IDLEMONITOR_H
