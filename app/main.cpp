#include <QtCore/QScopedPointer>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QApplication>

#include "loghandler.h"
#include "ui.h"
#include "settings.h"

#include "../systemmonitor/systemmonitor.h"
#include "../reportingsystem/reportingsystem.h"

#define ORG_NAME    "Organization"
#define APP_NAME    "Productivio"

extern QSettings *settings;


int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    settings = new QSettings(ORG_NAME, APP_NAME);

    QScopedPointer<LogHandler> logHandler (LogHandler::instance());

    QScopedPointer<SystemMonitor> systemMonitor (SystemMonitor::instance());
    systemMonitor.data()->start();

    QScopedPointer<Ui> ui (new Ui());
    ui.data()->bind(systemMonitor.data(), ReportingSystem::instance());
    ui.data()->show();

    return app.exec();
}

