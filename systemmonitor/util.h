#ifndef UTIL_H
#define UTIL_H

#include <QtCore/QString>
#include <QtCore/QRegExp>

bool    itemIsOfType(QString &item, char **types, unsigned int types_count);

#endif // UTIL_H
