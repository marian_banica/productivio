include(../common.pri);

TEMPLATE        = app

TARGET          = $$TARGET_NAME
DESTDIR         = $$TARGET_PATH

QT              += core gui widgets
LIBS            = -L$$LIB_PATH -lsystemmonitor -lreport
QMAKE_RPATHDIR  = $$LIB_PATH

SOURCES     += \
    main.cpp \
    loghandler.cpp \
    ui.cpp

HEADERS += \
    loghandler.h \
    ui.h \
    settings.h

RESOURCES += \
    res.qrc
