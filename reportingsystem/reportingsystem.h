#ifndef REPORTINGSYSTEM_H
#define REPORTINGSYSTEM_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QQueue>
#include <QtCore/QFile>
#include <QtNetwork/QNetworkAccessManager>


class ReportingSystem : public QObject
{
    Q_OBJECT

public:
    ~ReportingSystem();
    static ReportingSystem *instance();
    void report(QJsonObject activity);

private:
    ReportingSystem();
    void sendRequest(QByteArray *data);

    static ReportingSystem *_instance;
    QNetworkAccessManager   netManager;
    bool                    waitingForReply;
    QByteArray              *lastRequestData;
    QQueue<QByteArray*>     sendingQueue;
    QFile                   pendingRequestsFile;

private slots:
    void receivedReply(QNetworkReply *reply);

signals:
    void reported();

};

#endif // REPORTINGSYSTEM_H
