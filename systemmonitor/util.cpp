#include "util.h"

bool itemIsOfType(QString &item, char **types, unsigned int types_count)
{
    QRegExp regex;

    for(uint i=0; i<types_count; i++)
    {
        regex.setPattern(types[i]);
        if(regex.indexIn(item) >= 0)
            return true;
    }

    return false;
}
