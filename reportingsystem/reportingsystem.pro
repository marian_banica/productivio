
include(../common.pri)

TEMPLATE    = lib

TARGET      = report
DESTDIR     = $$LIB_PATH

QT          = core network

SOURCES += \
    reportingsystem.cpp

HEADERS += \
    reportingsystem.h

