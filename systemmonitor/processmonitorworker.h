#ifndef PROCESSMONITORWORKER_H
#define PROCESSMONITORWORKER_H

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QHash>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QRegExp>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include "core.h"


#define CHECK_INTERVAL 3


typedef struct{
    char    **data;
    size_t  len;
}TypeInfo;

typedef struct{
    long long duration;
    long long focusTime;
}ProcessInfo;


class ProcessMonitorWorker: public QObject
{
    Q_OBJECT

public:
    ProcessMonitorWorker(QHash<QString, long long> &, QHash<QString, long long> &, QHash<QString, ProcessInfo> &, QMutex &);

public slots:
    void run();
    void stop();
    void scanProcesses();

private:
    ERR     init();
    void    scanWindows();
    ERR     getProcessInfo(uint pid, const char *propRegex, QString &value);
    bool    processHasType(QString &processName, const char *type);

    Display *display;
    QTimer  timer;

    Atom    atomPID;
    Atom    atomFocusID;
    Atom    atomName;
    Atom    atomNetClientList;
    Atom    atomActiveWindow;
    Atom    utf8Atom;

    QHash<QString, TypeInfo> processTypes;

    static char *browsers[];
    static char *imClients[];

    QHash<QString, long long>   &browserUsage;
    QHash<QString, long long>   &imClientsUsage;
    QHash<QString, ProcessInfo> &processes;
    QMutex &workerMutex;
};

#endif // PROCESSMONITORWORKER_H
