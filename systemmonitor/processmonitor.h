#ifndef PROCESSMONITOR_H
#define PROCESSMONITOR_H

#include "BaseMonitor.h"
#include "processmonitorworker.h"


struct Process{
    QString             processName;
    unsigned long long  runningTime;
    unsigned long long  focusTime;
};


class ProcessMonitor : public BaseMonitor
{
    Q_OBJECT

private:
    ProcessMonitor();
    static ProcessMonitor *_instance;

    ERR getUserName(QString&);
    ERR getUserId(QString&, QString&);
    ERR startWorker(QString&);

    QString                 userID;
    ProcessMonitorWorker    worker;

    QHash<QString, long long>   browserUsage;
    QHash<QString, long long>   imClientsUsage;
    QHash<QString, ProcessInfo> processes;


public:
    static ProcessMonitor *instance();

    void            report(QJsonObject &);
    ERR             start();
    ERR             stop();
};

#endif // PROCESSMONITOR_H
